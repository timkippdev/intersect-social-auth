<?php

namespace TimKipp\Intersect\Social\Dao;

use TimKipp\Intersect\Database\Dao\AbstractDao;
use TimKipp\Intersect\Database\Query\SelectQuery;
use TimKipp\Intersect\Social\Domain\SocialAccount;

/**
 * Class SocialAccountDao
 * @package TimKipp\Intersect\Social\Dao
 */
class SocialAccountDao extends AbstractDao {

    /**
     * @return string
     */
    public function getDomainClass()
    {
        return SocialAccount::class;
    }

    /**
     * @param $providerId
     * @return mixed
     */
    public function getAllForProviderId($providerId)
    {
        $query = SelectQuery::table($this->getTable())->where('provider_id', (int) $providerId);
        return $this->getAdapter()->run($query)->getRecords();
    }

    /**
     * @param $email
     * @param $providerId
     * @return mixed|null
     */
    public function getByEmailAndProviderId($email, $providerId)
    {
        if (is_null($providerId))
        {
            return null;
        }

        $query = SelectQuery::table($this->getTable())->where('email', $email)->where('provider_id', $providerId)->limit(1);
        return $this->getAdapter()->run($query)->getFirstRecord();
    }

}