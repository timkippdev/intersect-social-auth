<?php

namespace TimKipp\Intersect\Social\Dao;

use TimKipp\Intersect\Database\Dao\AbstractDao;
use TimKipp\Intersect\Database\Query\SelectQuery;
use TimKipp\Intersect\Social\Domain\SocialProvider;

/**
 * Class SocialProviderDao
 * @package TimKipp\Intersect\Database\Dao
 */
class SocialProviderDao extends AbstractDao {

    /**
     * @return string
     */
    public function getDomainClass()
    {
        return SocialProvider::class;
    }

    /**
     * @param $providerName
     * @return SocialProvider|null
     */
    public function getByName($providerName)
    {
        $query = SelectQuery::table($this->getTable())->where('provider_name', $providerName)->limit(1);
        return $this->getAdapter()->run($query)->getFirstRecord();
    }

}