<?php

namespace TimKipp\Intersect\Social\Services;

use TimKipp\Intersect\Database\Adapters\AdapterInterface;
use TimKipp\Intersect\Domain\Account;
use TimKipp\Intersect\Services\AbstractService;
use TimKipp\Intersect\Services\AccountService;
use TimKipp\Intersect\Social\Clients\Facebook\FacebookClient;
use TimKipp\Intersect\Social\Clients\Google\GoogleClient;
use TimKipp\Intersect\Social\Clients\SocialClient;
use TimKipp\Intersect\Social\Clients\Twitter\TwitterClient;
use TimKipp\Intersect\Social\Dao\SocialAccountDao;
use TimKipp\Intersect\Social\Domain\SocialAccessToken;
use TimKipp\Intersect\Social\Domain\SocialAccount;
use TimKipp\Intersect\Social\Domain\SocialProvider;
use TimKipp\Intersect\Social\Domain\SocialUserDetails;
use TimKipp\Intersect\Social\Event\SocialAccountLoginEvent;
use TimKipp\Intersect\Social\Event\SocialAccountCreatedEvent;
use TimKipp\Intersect\Social\Event\SocialAccountLinkedEvent;
use TimKipp\Intersect\Social\Event\SocialAccountLoginFailedEvent;

/**
 * Class SocialAccountService
 * @package TimKipp\Intersect\Social\Services
 *
 * @method SocialAccountDao getDao
 */
class SocialAccountService extends AbstractService {

    /** @var AccountService $accountService */
    private $accountService;

    /** @var FacebookClient $facebookClient */
    private $facebookClient;

    /** @var GoogleClient $googleClient */
    private $googleClient;

    /** @var SocialProviderService $socialProviderService */
    private $socialProviderService;

    /** @var TwitterClient $twitterClient */
    private $twitterClient;

    /**
     * SocialAccountService constructor.
     * @param AdapterInterface $databaseAdapter
     */
    public function __construct(AdapterInterface $databaseAdapter)
    {
        parent::__construct($databaseAdapter);

        $this->setDao(new SocialAccountDao($databaseAdapter));

        $this->accountService = new AccountService($databaseAdapter);
        $this->socialProviderService = new SocialProviderService($databaseAdapter);
    }

    /**
     * @param SocialProviderService $socialProviderService
     */
    public function setSocialProviderService(SocialProviderService $socialProviderService)
    {
        $this->socialProviderService = $socialProviderService;
    }

    /**
     * @param AccountService $accountService
     */
    public function setAccountService(AccountService $accountService)
    {
        $this->accountService = $accountService;
    }

    /**
     * @param FacebookClient $facebookClient
     */
    public function setFacebookClient(FacebookClient $facebookClient)
    {
        $this->facebookClient = $facebookClient;
    }

    /**
     * @param TwitterClient $twitterClient
     */
    public function setTwitterClient(TwitterClient $twitterClient)
    {
        $this->twitterClient = $twitterClient;
    }

    /**
     * @param GoogleClient $googleClient
     */
    public function setGoogleClient(GoogleClient $googleClient)
    {
        $this->googleClient = $googleClient;
    }

    /**
     * @param $providerId
     * @return mixed
     */
    public function getAllForProviderId($providerId)
    {
        return $this->getDao()->getAllForProviderId($providerId);
    }

    /**
     * @param $email
     * @param $providerName
     * @return SocialAccount|null
     */
    public function getByEmailAndProviderName($email, $providerName)
    {
        $provider = $this->socialProviderService->getByName($providerName);

        return $this->getDao()->getByEmailAndProviderId($email, (!is_null($provider) ? $provider->getProviderId() : null));
    }

    /**
     * @param Account $account
     * @param $providerName
     * @param $providerAccountId
     * @param SocialAccessToken $accessToken
     * @param $linkingToExistingAccount
     * @return mixed|null
     * @throws \TimKipp\Intersect\Validation\ValidationException
     */
    public function createFromAccount(Account $account, $providerName, $providerAccountId, SocialAccessToken $accessToken, $linkingToExistingAccount)
    {
        $socialAccount = new SocialAccount();
        $socialAccount->setAccountId($account->getAccountId());
        $socialAccount->setEmail($account->getEmail());
        $socialAccount->setProviderAccountId($providerAccountId);
        $socialAccount->setProviderAccessToken($accessToken->getAccessToken());
        $socialAccount->setProviderAccessTokenSecret($accessToken->getAccessTokenSecret());
        $socialAccount->setStatus(1);

        $provider = $this->socialProviderService->getByName($providerName);

        $socialAccount->setProviderId($provider->getProviderId());

        $createdSocialAccount = $this->create($socialAccount);

        if (!is_null($createdSocialAccount))
        {
            if ($linkingToExistingAccount)
            {
                $this->getEventDispatcher()->dispatch(new SocialAccountLinkedEvent($account, $createdSocialAccount), 'Social account was linked successfully.');
            }
            else
            {
                $this->getEventDispatcher()->dispatch(new SocialAccountCreatedEvent($createdSocialAccount), 'Social account created successfully.');
            }
        }

        return $createdSocialAccount;
    }

    /**
     * @return bool
     */
    public function isLoginWithFacebookDetected()
    {
        return (isset($_GET['code'], $_GET['state']));
    }

    /**
     * @param array $scopeParameters
     * @return string
     * @throws \Exception
     */
    public function getFacebookLoginUrl(array $scopeParameters = array('email'))
    {
        if (is_null($this->facebookClient))
        {
            throw new \Exception('FacebookClient not set properly');
        }

        return $this->facebookClient->getLoginUrl($scopeParameters);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function loginWithFacebook($storeCookie = true, $shouldCreateAccount = true)
    {
        if (is_null($this->facebookClient))
        {
            throw new \Exception('FacebookClient not set properly');
        }

        return $this->handleSocialLogin($this->facebookClient, SocialProvider::PROVIDER_NAME_FACEBOOK, $storeCookie, $shouldCreateAccount);
    }

    /**
     * @return bool
     */
    public function isLoginWithTwitterDetected()
    {
        return (isset($_GET['oauth_token'], $_GET['oauth_verifier']));
    }

    /**
     * @return string
     * @throws \Exception
     * @throws \TimKipp\Intersect\Social\SocialClientException
     */
    public function getTwitterLoginUrl()
    {
        if (is_null($this->twitterClient))
        {
            throw new \Exception('TwitterClient not set properly');
        }

        return $this->twitterClient->getLoginUrl();
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function loginWithTwitter($storeCookie = true, $shouldCreateAccount = true)
    {
        if (is_null($this->twitterClient))
        {
            throw new \Exception('TwitterClient not set properly');
        }

        return $this->handleSocialLogin($this->twitterClient, SocialProvider::PROVIDER_NAME_TWITTER, $storeCookie, $shouldCreateAccount);
    }

    /**
     * @return bool
     */
    public function isLoginWithGoogleDetected()
    {
        return (isset($_GET['code']));
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getGoogleLoginUrl()
    {
        if (is_null($this->googleClient))
        {
            throw new \Exception('GoogleClient not set properly');
        }

        return $this->googleClient->getLoginUrl();
    }

    /**
     * @param bool $storeCookie
     * @param bool $shouldCreateAccount
     * @return bool
     * @throws \Exception
     */
    public function loginWithGoogle($storeCookie = true, $shouldCreateAccount = true)
    {
        if (is_null($this->googleClient))
        {
            throw new \Exception('GoogleClient not set properly');
        }

        return $this->handleSocialLogin($this->googleClient, SocialProvider::PROVIDER_NAME_GOOGLE, $storeCookie, $shouldCreateAccount);
    }

    /**
     * @param SocialUserDetails $socialUserDetails
     * @return Account
     * @throws \Exception
     */
    private function createAccountFromSocialUserDetails(SocialUserDetails $socialUserDetails, $providerName)
    {
        $newAccount = new Account();
        $newAccount->setEmail($socialUserDetails->getEmail());
        $newAccount->setFirstName($socialUserDetails->getFirstName());
        $newAccount->setLastName($socialUserDetails->getLastName());
        $newAccount->setRegistrationSource($providerName);

        $createdAccount = $this->accountService->createWithoutPassword($newAccount);

        if (is_null($createdAccount))
        {
            throw new \Exception('There was a problem creating your account.');
        }

        return $createdAccount;
    }

    /**
     * @param Account|null $account
     * @return bool
     */
    private function handleFailedLogin(Account $account = null)
    {
        $this->getEventDispatcher()->dispatch(new SocialAccountLoginFailedEvent(), 'There was a problem logging in with your social account.');
    }

    /**
     * @param Account $account
     */
    private function handleLoginSuccess(Account $account)
    {
        $this->getEventDispatcher()->dispatch(new SocialAccountLoginEvent($account), 'Social account logged in successfully.');
    }

    /**
     * SOCIAL LOGIN SCENARIOS
     * 1) social account exists but normal account does not - throw exception
     * 2) no social account or normal account exists - create account then create social account
     * 3) normal account exists but no social account - create social account
     * 4) both social and normal account exists - do nothing
     *
     * @param SocialClient $socialClient
     * @param $providerName
     * @return bool
     * @throws \Exception
     */
    private function handleSocialLogin(SocialClient $socialClient, $providerName, $storeCookie = true, $shouldCreateAccount = true)
    {
        $accessToken = $socialClient->getAccessToken();

        if (is_null($accessToken) || is_null($accessToken->getAccessToken()))
        {
            return false;
        }

        $userDetails = $socialClient->getUserDetails($accessToken);

        $exisingSocialAccount = $this->getByEmailAndProviderName($userDetails->getEmail(), $providerName);
        $existingAccount = $this->accountService->getByEmail($userDetails->getEmail());

        if (is_null($existingAccount) && !is_null($exisingSocialAccount))
        {
            throw new \Exception('Social account (' . $exisingSocialAccount->getId() . ') does not have an existing account associated with it. This should not happen ever.');
        }

        if (is_null($existingAccount) && is_null($exisingSocialAccount))
        {
            if (!$shouldCreateAccount)
            {
                return false;
            }

            $createdAccount = $this->createAccountFromSocialUserDetails($userDetails, $providerName);

            $newSocialAccount = new SocialAccount();
            $newSocialAccount->setEmail($userDetails->getEmail());

            $createdSocialAccount = $this->createFromAccount($createdAccount, $providerName, $userDetails->getId(), $accessToken, false);
            if (is_null($createdSocialAccount))
            {
                throw new \Exception('Something went wrong creating the new social account');
            }

            $existingAccount = $createdAccount;
        }
        else if (!is_null($existingAccount) && is_null($exisingSocialAccount))
        {
            $createdSocialAccount = $this->createFromAccount($existingAccount, $providerName, $userDetails->getId(), $accessToken, true);

            if (is_null($createdSocialAccount))
            {
                throw new \Exception('Something went wrong creating the new social account');
            }
        }

        $loginSuccess = $this->accountService->login($existingAccount, $storeCookie);

        if ($loginSuccess)
        {
            $this->handleLoginSuccess($existingAccount);
        }
        else
        {
            $this->handleFailedLogin($existingAccount);
        }

        return $loginSuccess;
    }

}