<?php

namespace TimKipp\Intersect\Social\Services;

use TimKipp\Intersect\Database\Adapters\AdapterInterface;
use TimKipp\Intersect\Services\AbstractService;
use TimKipp\Intersect\Social\Dao\SocialProviderDao;
use TimKipp\Intersect\Social\Domain\SocialProvider;

/**
 * Class SocialProviderService
 * @package TimKipp\Intersect\Social\Services
 *
 * @method SocialProviderDao getDao
 */
class SocialProviderService extends AbstractService {

    /**
     * SocialProviderService constructor.
     * @param AdapterInterface $databaseAdapter
     */
    public function __construct(AdapterInterface $databaseAdapter)
    {
        parent::__construct($databaseAdapter);

        $this->setDao(new SocialProviderDao($databaseAdapter));
    }

    /**
     * @param $providerName
     * @return null|SocialProvider
     */
    public function getByName($providerName)
    {
        return $this->getDao()->getByName($providerName);
    }

}