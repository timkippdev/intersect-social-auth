<?php

namespace TimKipp\Intersect\Social\Controllers;

use TimKipp\Intersect\Controllers\AbstractBaseController;
use TimKipp\Intersect\Social\Domain\SocialProvider;
use TimKipp\Intersect\Social\Services\SocialAccountService;

class SocialLoginController extends AbstractBaseController {

    private $socialAccountService;

    public function __construct($controllerParameters = array())
    {
        parent::__construct($controllerParameters);

        if (array_key_exists('socialAccountService', $controllerParameters) && $controllerParameters['socialAccountService'] instanceof SocialAccountService)
        {
            $this->socialAccountService = $controllerParameters['socialAccountService'];
        }
    }

    /**
     * @return SocialAccountService
     */
    protected function getSocialAccountService()
    {
        return (!is_null($this->socialAccountService) ? $this->socialAccountService : new SocialAccountService($this->getDatabaseAdapter()));
    }

    /**
     * @throws \Exception
     */
    public function processSocialLoginCallback()
    {
        if ($this->getSocialAccountService()->isLoginWithFacebookDetected())
        {
            $this->getSocialAccountService()->loginWithFacebook();
        }
        else if ($this->getSocialAccountService()->isLoginWithTwitterDetected())
        {
            $this->getSocialAccountService()->loginWithTwitter();
        }
        else if ($this->getSocialAccountService()->isLoginWithGoogleDetected())
        {
            $this->getSocialAccountService()->loginWithGoogle();
        }
    }

    /**
     * @param $providerName
     * @param bool $returnProviderUrlOnly
     * @return null|string
     * @throws \TimKipp\Intersect\Social\SocialClientException
     */
    public function redirectToSocialProvider($providerName, $returnProviderUrlOnly = false)
    {
        $providerUrl = null;

        switch ($providerName)
        {
            case SocialProvider::PROVIDER_NAME_FACEBOOK:
                $providerUrl = $this->getSocialAccountService()->getFacebookLoginUrl();
                break;
            case SocialProvider::PROVIDER_NAME_TWITTER:
                $providerUrl = $this->getSocialAccountService()->getTwitterLoginUrl();
                break;
            case SocialProvider::PROVIDER_NAME_GOOGLE:
                $providerUrl = $this->getSocialAccountService()->getGoogleLoginUrl();
                break;
            default:
                throw new \Exception('Unsupported social provider');
        }

        if ($returnProviderUrlOnly)
        {
            return $providerUrl;
        }

        header('Location: ' . $providerUrl);
        exit();
    }
    
}