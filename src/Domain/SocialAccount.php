<?php

namespace TimKipp\Intersect\Social\Domain;

use TimKipp\Intersect\Domain\AbstractTemporalDomain;

/**
 * Class SocialAccount
 * @package TimKipp\Intersect\Social\Domain
 */
class SocialAccount extends AbstractTemporalDomain {

    public $id;
    public $providerId;
    public $providerAccountId;
    public $providerAccessToken;
    public $providerAccessTokenSecret;
    public $accountId;
    public $email;
    public $status;
    public $dateCreated;
    public $dateUpdated;

    /**
     * @return mixed|string
     */
    public function getTable()
    {
        return 'social_account';
    }

    /**
     * @return string
     */
    public function getPrimaryKeyColumn()
    {
        return 'id';
    }

    /**
     * @return array
     */
    public static function getColumnMappings()
    {
        return array(
            'id' => 'id',
            'provider_id' => 'providerId',
            'provider_account_id' => 'providerAccountId',
            'provider_access_token' => 'providerAccessToken',
            'provider_access_token_secret' => 'providerAccessTokenSecret',
            'account_id' => 'accountId',
            'email' => 'email',
            'status' => 'status',
            'date_created' => 'dateCreated',
            'date_updated' => 'dateUpdated'
        );
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getProviderId()
    {
        return $this->providerId;
    }

    /**
     * @param mixed $providerId
     */
    public function setProviderId($providerId)
    {
        $this->providerId = $providerId;
    }

    /**
     * @return mixed
     */
    public function getProviderAccountId()
    {
        return $this->providerAccountId;
    }

    /**
     * @param mixed $providerAccountId
     */
    public function setProviderAccountId($providerAccountId)
    {
        $this->providerAccountId = $providerAccountId;
    }

    /**
     * @return mixed
     */
    public function getProviderAccessToken()
    {
        return $this->providerAccessToken;
    }

    /**
     * @param mixed $providerAccessToken
     */
    public function setProviderAccessToken($providerAccessToken)
    {
        $this->providerAccessToken = $providerAccessToken;
    }

    /**
     * @return mixed
     */
    public function getProviderAccessTokenSecret()
    {
        return $this->providerAccessTokenSecret;
    }

    /**
     * @param mixed $providerAccessTokenSecret
     */
    public function setProviderAccessTokenSecret($providerAccessTokenSecret)
    {
        $this->providerAccessTokenSecret = $providerAccessTokenSecret;
    }

    /**
     * @return mixed
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * @param mixed $accountId
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}