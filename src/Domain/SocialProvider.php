<?php

namespace TimKipp\Intersect\Social\Domain;

use TimKipp\Intersect\Domain\AbstractDomain;

/**
 * Class SocialProvider
 * @package TimKipp\Intersect\Social\Domain
 */
class SocialProvider extends AbstractDomain {

    const PROVIDER_NAME_FACEBOOK = 'facebook';
    const PROVIDER_NAME_TWITTER = 'twitter';
    const PROVIDER_NAME_GOOGLE = 'google';

    public $providerId;
    public $providerName;

    /**
     * @return mixed|string
     */
    public function getTable()
    {
        return 'social_provider';
    }

    /**
     * @return string
     */
    public function getPrimaryKeyColumn()
    {
        return 'provider_id';
    }

    /**
     * @return array
     */
    public static function getColumnMappings()
    {
        return array(
            'provider_id' => 'providerId',
            'provider_name' => 'providerName'
        );
    }

    /**
     * @return mixed
     */
    public function getProviderId()
    {
        return $this->providerId;
    }

    /**
     * @param mixed $providerId
     */
    public function setProviderId($providerId)
    {
        $this->providerId = $providerId;
    }

    /**
     * @return mixed
     */
    public function getProviderName()
    {
        return $this->providerName;
    }

    /**
     * @param mixed $providerName
     */
    public function setProviderName($providerName)
    {
        $this->providerName = $providerName;
    }

}