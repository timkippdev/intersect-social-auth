<?php

class Migration_1_0_0_SocialAuth extends \TimKipp\Intersect\Migration\AbstractMigration {

    public function getVersion()
    {
        return '1.0.0-intersect-social-auth';
    }

    public function migrateUp()
    {
        $this->getAdapter()->run(\TimKipp\Intersect\Database\Query\RawQuery::init("
            CREATE TABLE IF NOT EXISTS `social_provider` (
              `provider_id` INT(11) NOT NULL AUTO_INCREMENT,
              `provider_name` VARCHAR(255) NOT NULL,
              PRIMARY KEY (`provider_id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=UTF8;
        "));

        $this->getAdapter()->run(\TimKipp\Intersect\Database\Query\RawQuery::init("
            CREATE TABLE IF NOT EXISTS `social_account` (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `provider_id` INT(4) NOT NULL,
              `provider_account_id` VARCHAR(255),
              `provider_access_token` TEXT,
              `provider_access_token_secret` TEXT,
              `account_id` INT(11) NOT NULL,
              `email` VARCHAR(255) NOT NULL,
              `status` TINYINT(2) NOT NULL DEFAULT 1,
              `date_created` DATETIME NOT NULL,
              `date_updated` DATETIME,
              PRIMARY KEY (`id`),
              UNIQUE KEY unique_idx_email_provider_id (`email`, `provider_id`),
              FOREIGN KEY (`account_id`) REFERENCES account (`account_id`),
              FOREIGN KEY (`provider_id`) REFERENCES social_provider (`provider_id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=UTF8;
        "));

        $this->getAdapter()->run(\TimKipp\Intersect\Database\Query\InsertQuery::table('social_provider')->values(array('provider_name' => 'facebook')));
        $this->getAdapter()->run(\TimKipp\Intersect\Database\Query\InsertQuery::table('social_provider')->values(array('provider_name' => 'twitter')));
        $this->getAdapter()->run(\TimKipp\Intersect\Database\Query\InsertQuery::table('social_provider')->values(array('provider_name' => 'google')));
    }

    public function migrateDown()
    {
        $this->getAdapter()->dropTable("social_account");
        $this->getAdapter()->dropTable("social_provider");
    }

}