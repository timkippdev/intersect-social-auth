<?php

namespace TimKipp\Intersect\Social\Event;

use TimKipp\Intersect\Event\Event;

/**
 * Class SocialAccountCreatedEvent
 * @package TimKipp\Intersect\Social\Event
 */
class SocialAccountCreatedEvent extends AbstractSocialAccountEvent {

    const SOCIAL_ACCOUNT_CREATED = 'SOCIAL_ACCOUNT_CREATED';

    /**
     * @return string
     */
    public function getName()
    {
        return self::SOCIAL_ACCOUNT_CREATED;
    }

}