<?php

namespace TimKipp\Intersect\Social\Event;

use TimKipp\Intersect\Event\Event;

/**
 * Class SocialAccountLoginFailedEvent
 * @package TimKipp\Intersect\Social\Event
 */
class SocialAccountLoginFailedEvent extends AbstractSocialAccountEvent {

    const SOCIAL_ACCOUNT_LOGIN_FAILED = 'SOCIAL_ACCOUNT_LOGIN_FAILED';

    /**
     * @return string
     */
    public function getName()
    {
        return self::SOCIAL_ACCOUNT_LOGIN_FAILED;
    }

}