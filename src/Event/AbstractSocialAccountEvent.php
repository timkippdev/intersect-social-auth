<?php

namespace TimKipp\Intersect\Social\Event;

use TimKipp\Intersect\Event\Event;
use TimKipp\Intersect\Social\Domain\SocialAccount;

/**
 * Class AbstractSocialAccountEvent
 * @package TimKipp\Intersect\Social\Event
 */
abstract class AbstractSocialAccountEvent extends Event {

    private $socialAccount;

    /**
     * AbstractSocialAccountEvent constructor.
     * @param SocialAccount $socialAccount
     */
    public function __construct(SocialAccount $socialAccount = null)
    {
        $this->socialAccount = $socialAccount;
    }

    /**
     * @return SocialAccount
     */
    public function getSocialAccount()
    {
        return $this->socialAccount;
    }

}