<?php

namespace TimKipp\Intersect\Social\Event;

use TimKipp\Intersect\Domain\Account;
use TimKipp\Intersect\Event\Event;
use TimKipp\Intersect\Social\Domain\SocialAccount;

/**
 * Class SocialAccountLinkedEvent
 * @package TimKipp\Intersect\Social\Event
 */
class SocialAccountLinkedEvent extends AbstractSocialAccountEvent {

    const SOCIAL_ACCOUNT_LINKED = 'SOCIAL_ACCOUNT_LINKED';

    private $accountLinkedTo;

    /**
     * SocialAccountLinkedEvent constructor.
     * @param Account $account
     * @param SocialAccount $createdSocialAccount
     */
    public function __construct(Account $account, SocialAccount $createdSocialAccount)
    {
        parent::__construct($createdSocialAccount);
        $this->accountLinkedTo = $account;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return self::SOCIAL_ACCOUNT_LINKED;
    }

    /**
     * @return Account
     */
    public function getAccountLinkedTo()
    {
        return $this->accountLinkedTo;
    }

}