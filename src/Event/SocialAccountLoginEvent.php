<?php

namespace TimKipp\Intersect\Social\Event;

use TimKipp\Intersect\Event\AbstractAccountEvent;
use TimKipp\Intersect\Event\Event;

/**
 * Class AccountSocialLoginEvent
 * @package TimKipp\Intersect\Social\Event
 */
class SocialAccountLoginEvent extends AbstractAccountEvent {

    const SOCIAL_ACCOUNT_LOGIN = 'SOCIAL_ACCOUNT_LOGIN';

    /**
     * @return string
     */
    public function getName()
    {
        return self::SOCIAL_ACCOUNT_LOGIN;
    }

}