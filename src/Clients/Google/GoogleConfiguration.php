<?php

namespace TimKipp\Intersect\Social\Clients\Google;

/**
 * Class GoogleConfiguration
 * @package TimKipp\Intersect\Social\Clients\Google
 */
class GoogleConfiguration {

    private $callbackUrl;
    private $clientId;
    private $clientSecret;
    private $scopes;

    /**
     * GoogleConfiguration constructor.
     * @param $clientId
     * @param $clientSecret
     * @param $callbackUrl
     * @param array $scopes
     */
    public function __construct($clientId, $clientSecret, $callbackUrl, array $scopes = array())
    {
        $this->callbackUrl = $callbackUrl;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->scopes = $scopes;
    }

    /**
     * @return array
     */
    public function getScopes()
    {
        return $this->scopes;
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @return mixed
     */
    public function getClientSecret()
    {
        return $this->clientSecret;
    }

    /**
     * @return mixed
     */
    public function getCallbackUrl()
    {
        return $this->callbackUrl;
    }

}