<?php

namespace TimKipp\Intersect\Social\Clients\Google;

use TimKipp\Intersect\Social\Clients\SocialClient;
use TimKipp\Intersect\Social\Domain\SocialAccessToken;
use TimKipp\Intersect\Social\Domain\SocialUserDetails;
use TimKipp\Intersect\Social\SocialClientException;

/**
 * Class GoogleClient
 * @package TimKipp\Intersect\Social\Clients\Google;
 */
class GoogleClient implements SocialClient {

    private $oauthClient;

    private $oauthService;

    /**
     * GoogleClient constructor.
     * @param GoogleConfiguration $googleConfiguration
     * @throws SocialClientException
     */
    public function __construct(GoogleConfiguration $googleConfiguration)
    {
        if (is_null($googleConfiguration->getClientId()) || trim($googleConfiguration->getClientId()) == '')
        {
            throw new SocialClientException("GoogleConfiguration clientId is not set");
        }

        if (is_null($googleConfiguration->getClientSecret()) || trim($googleConfiguration->getClientSecret()) == '')
        {
            throw new SocialClientException("GoogleConfiguration clientSecret is not set");
        }

        if (is_null($googleConfiguration->getCallbackUrl()) || trim($googleConfiguration->getCallbackUrl()) == '')
        {
            throw new SocialClientException("GoogleConfiguration callbackUrl is not set");
        }

        $client = new \Google_Client();
        $client->setClientId($googleConfiguration->getClientId());
        $client->setClientSecret($googleConfiguration->getClientSecret());
        $client->setRedirectUri($googleConfiguration->getCallbackUrl());
        $client->setScopes($googleConfiguration->getScopes());

        $this->oauthClient = $client;
        $this->oauthService = new \Google_Service_Oauth2($client);
    }

    /**
     * @return bool|null|SocialAccessToken
     * @throws \TimKipp\Intersect\Social\SocialClientException
     */
    public function getAccessToken()
    {
        if (!isset($_GET['code']) || trim($_GET['code']) == '')
        {
            return null;
        }

        $authCode = $_GET['code'];
        $rawAccessToken = $this->oauthClient->fetchAccessTokenWithAuthCode($authCode);

        if (is_null($rawAccessToken))
        {
            return null;
        }

        if (array_key_exists('error', $rawAccessToken))
        {
            throw new SocialClientException($rawAccessToken['error_description']);
        }

        return new SocialAccessToken($rawAccessToken['access_token']);
    }

    /**
     * @param array $parameters
     * @return string
     */
    public function getLoginUrl(array $parameters = array())
    {
        return $this->oauthClient->createAuthUrl();
    }

    /**
     * @param SocialAccessToken|null $accessToken
     * @return mixed|null|SocialUserDetails
     * @throws SocialClientException
     */
    public function getUserDetails(SocialAccessToken $accessToken = null)
    {
        if (is_null($accessToken->getAccessToken()))
        {
            return null;
        }

        $this->oauthClient->setAccessToken($accessToken->getAccessToken());

        $googleUser = $this->oauthService->userinfo->get();

        if (is_null($googleUser))
        {
            throw new SocialClientException('Something went wrong retrieving user details from Google');
        }

        if (is_null($googleUser->getEmail()) || trim($googleUser->getEmail()) == '')
        {
            throw new SocialClientException('A valid Google email could not be found');
        }

        $userDetails = new SocialUserDetails();
        $userDetails->setId($googleUser->getId());
        $userDetails->setEmail($googleUser->getEmail());

        return $userDetails;
    }

    /**
     * @return \Google_Client
     */
    protected function getOAuthClient()
    {
        return $this->oauthClient;
    }

    /**
     * @param \Google_Client $client
     */
    protected function setOAuthClient(\Google_Client $client)
    {
        $this->oauthClient = $client;
    }

    /**
     * @return \Google_Service_Oauth2
     */
    protected function getOAuthService()
    {
        return $this->oauthService;
    }

    /**
     * @param \Google_Service $service
     */
    protected function setOAuthService(\Google_Service $service)
    {
        $this->oauthService = $service;
    }

}