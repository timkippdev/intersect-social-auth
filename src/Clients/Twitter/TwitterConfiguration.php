<?php

namespace TimKipp\Intersect\Social\Clients\Twitter;

/**
 * Class TwitterConfiguration
 * @package TimKipp\Intersect\Social\Clients\Twitter
 */
class TwitterConfiguration {

    private $callbackUrl;
    private $consumerKey;
    private $consumerSecret;

    /**
     * TwitterConfiguration constructor.
     * @param $consumerKey
     * @param $consumerSecret
     * @param $callbackUrl
     */
    public function __construct($consumerKey, $consumerSecret, $callbackUrl)
    {
        $this->callbackUrl = $callbackUrl;
        $this->consumerKey = $consumerKey;
        $this->consumerSecret = $consumerSecret;
    }

    /**
     * @return mixed
     */
    public function getCallbackUrl()
    {
        return $this->callbackUrl;
    }

    /**
     * @return mixed
     */
    public function getConsumerKey()
    {
        return $this->consumerKey;
    }

    /**
     * @return mixed
     */
    public function getConsumerSecret()
    {
        return $this->consumerSecret;
    }

}