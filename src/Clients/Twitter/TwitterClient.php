<?php

namespace TimKipp\Intersect\Social\Clients\Twitter;

use Abraham\TwitterOAuth\TwitterOAuth;
use Abraham\TwitterOAuth\TwitterOAuthException;
use TimKipp\Intersect\Helper\SessionManager;
use TimKipp\Intersect\Social\Clients\SocialClient;
use TimKipp\Intersect\Social\Domain\SocialAccessToken;
use TimKipp\Intersect\Social\Domain\SocialUserDetails;
use TimKipp\Intersect\Social\SocialClientException;

/**
 * Class TwitterClient
 * @package TimKipp\Intersect\Social\Clients\Twitter
 */
class TwitterClient implements SocialClient {

    private $callbackUrl;

    /** @var SessionManager $sessionManager */
    private $sessionManager;

    /** @var TwitterOAuth $twitterOAuth */
    private $twitterOAuth;

    /**
     * TwitterClient constructor.
     * @param TwitterConfiguration $twitterConfiguration
     * @param SessionManager $sessionManager
     * @throws SocialClientException
     */
    public function __construct(TwitterConfiguration $twitterConfiguration, SessionManager $sessionManager)
    {
        $this->sessionManager = $sessionManager;
        $this->callbackUrl = $twitterConfiguration->getCallbackUrl();

        if (is_null($twitterConfiguration->getConsumerKey()) || trim($twitterConfiguration->getConsumerKey()) == '')
        {
            throw new SocialClientException("TwitterConfiguration consumerKey is not set");
        }

        if (is_null($twitterConfiguration->getConsumerSecret()) || trim($twitterConfiguration->getConsumerSecret()) == '')
        {
            throw new SocialClientException("TwitterConfiguration consumerSecret is not set");
        }

        if (is_null($twitterConfiguration->getCallbackUrl()) || trim($twitterConfiguration->getCallbackUrl()) == '')
        {
            throw new SocialClientException("TwitterConfiguration callbackUrl is not set");
        }

        $this->twitterOAuth = new TwitterOAuth($twitterConfiguration->getConsumerKey(), $twitterConfiguration->getConsumerSecret());
    }

    /**
     * @return null|SocialAccessToken
     * @throws SocialClientException
     */
    public function getAccessToken()
    {
        $oauthTokenFromSession = $this->sessionManager->readAndClear('twitter_oauth_token');
        $oauthTokenSecretFromSession = $this->sessionManager->readAndClear('twitter_oauth_token_secret');
        $oauthToken = (isset($_GET['oauth_token']) ? $_GET['oauth_token'] : '');
        $oauthVerifier = (isset($_GET['oauth_verifier']) ? $_GET['oauth_verifier'] : '');

        if ((is_null($oauthTokenFromSession) || is_null($oauthTokenSecretFromSession)) || ($oauthTokenFromSession !== $oauthToken))
        {
            throw new SocialClientException('Invalid OAuth token data');
        }

        $this->twitterOAuth->setOauthToken($oauthTokenFromSession, $oauthTokenSecretFromSession);

        try {
            $accessToken = $this->twitterOAuth->oauth("oauth/access_token", array('oauth_verifier' => $oauthVerifier));
        } catch (TwitterOAuthException $e) {
            throw new SocialClientException($e->getMessage());
        }

        return new SocialAccessToken($accessToken['oauth_token'], $accessToken['oauth_token_secret']);
    }

    /**
     * @param array $parameters
     * @return string
     * @throws SocialClientException
     */
    public function getLoginUrl(array $parameters = array())
    {
        try {
            $requestTokens = $this->twitterOAuth->oauth('oauth/request_token', array('oauth_callback' => $this->callbackUrl));
        } catch (TwitterOAuthException $e) {
            throw new SocialClientException($e->getMessage());
        }

        $oauthToken = $requestTokens['oauth_token'];
        $oauthTokenSecret = $requestTokens['oauth_token_secret'];

        $this->sessionManager->write('twitter_oauth_token', $oauthToken);
        $this->sessionManager->write('twitter_oauth_token_secret', $oauthTokenSecret);

        return $this->twitterOAuth->url('oauth/authorize', array('oauth_token' => $oauthToken));
    }

    /**
     * @param SocialAccessToken|null $accessToken
     * @return mixed|SocialUserDetails
     * @throws SocialClientException
     */
    public function getUserDetails(SocialAccessToken $accessToken = null)
    {
        $this->twitterOAuth->setOauthToken($accessToken->getAccessToken(), $accessToken->getAccessTokenSecret());

        $parameters = array(
            'include_email' => 'true',
            'include_entities' => 'false',
            'skip_status' => 'true'
        );

        $twitterUser = $this->twitterOAuth->get('account/verify_credentials', $parameters);

        if (is_null($twitterUser))
        {
            throw new SocialClientException('Something went wrong retrieving user details from Twitter');
        }

        if (is_null($twitterUser->email) || trim($twitterUser->email) == '')
        {
            throw new SocialClientException('A valid Twitter email could not be found');
        }

        $userDetails = new SocialUserDetails();
        $userDetails->setId($twitterUser->id);
        $userDetails->setEmail($twitterUser->email);

        return $userDetails;
    }

    protected function getTwitterOAuth()
    {
        return $this->twitterOAuth;
    }

    protected function setTwitterOAuth(TwitterOAuth $twitterOAuth)
    {
        $this->twitterOAuth = $twitterOAuth;
    }

}