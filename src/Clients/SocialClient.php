<?php

namespace TimKipp\Intersect\Social\Clients;

use TimKipp\Intersect\Social\Domain\SocialAccessToken;

/**
 * Interface SocialClient
 * @package TimKipp\Intersect\Social\Clients
 */
interface SocialClient {

    /**
     * @return SocialAccessToken|null
     */
    public function getAccessToken();

    /**
     * @param array $parameters
     * @return string
     */
    public function getLoginUrl(array $parameters = array());

    /**
     * @param $accessToken
     * @return mixed
     */
    public function getUserDetails(SocialAccessToken $accessToken = null);

}