<?php

namespace TimKipp\Intersect\Tests\IT\Services;

use TimKipp\Intersect\Domain\Account;
use TimKipp\Intersect\Event\Event;
use TimKipp\Intersect\Social\Domain\SocialAccessToken;
use TimKipp\Intersect\Social\Domain\SocialProvider;
use TimKipp\Intersect\Social\Event\SocialAccountCreatedEvent;
use TimKipp\Intersect\Social\Event\SocialAccountLinkedEvent;
use TimKipp\Intersect\Social\Services\SocialAccountService;
use TimKipp\Intersect\Tests\IT\BaseITTest;
use TimKipp\Intersect\Tests\Stubs\TestAccount;
use TimKipp\Intersect\Tests\Stubs\TestListener;
use TimKipp\Intersect\Tests\Stubs\TestSocialAccount;

class SocialAccountServiceTest extends BaseITTest {

    /** @var SocialAccountService $socialAccountService */
    private $socialAccountService;

    /** @var Account $createdAccount */
    private $createdAccount;

    protected function setUp()
    {
        parent::setUp();

        $this->socialAccountService = new SocialAccountService(self::$DB);
        $this->createdAccount = $this->accountService->create(new TestAccount());
    }

    public function test_createRecord()
    {
        $sampleSocialAccount = new TestSocialAccount($this->createdAccount->getAccountId());

        $createdSocialAccount = $this->socialAccountService->create($sampleSocialAccount);

        $this->assertNotNull($createdSocialAccount);
        $this->assertNotNull($createdSocialAccount->getId());
        $this->assertEquals($sampleSocialAccount->getEmail(), $sampleSocialAccount->getEmail());
        $this->assertEquals($sampleSocialAccount->getProviderId(), $sampleSocialAccount->getProviderId());
        $this->assertEquals($sampleSocialAccount->getProviderAccessToken(), $sampleSocialAccount->getProviderAccessToken());
        $this->assertEquals($sampleSocialAccount->getProviderAccountId(), $sampleSocialAccount->getProviderAccountId());
        $this->assertEquals($sampleSocialAccount->getStatus(), $sampleSocialAccount->getStatus());
        $this->assertEquals($sampleSocialAccount->getAccountId(), $sampleSocialAccount->getAccountId());
    }

    public function test_getAllForProviderId()
    {
        $results = $this->socialAccountService->getAllForProviderId(2);
        $this->assertCount(0, $results);

        $this->socialAccountService->create(new TestSocialAccount($this->createdAccount->getAccountId()));

        $results = $this->socialAccountService->getAllForProviderId(1);
        $this->assertNotNull($results);
        $this->assertTrue(count($results) > 0);
    }

    public function test_getByEmailAndProviderName()
    {
        $result = $this->socialAccountService->getByEmailAndProviderName('doesnotexist', 'test');
        $this->assertNull($result);

        $createdSocialAccount = $this->socialAccountService->create(new TestSocialAccount($this->createdAccount->getAccountId()));

        $result = $this->socialAccountService->getByEmailAndProviderName($createdSocialAccount->getEmail(), SocialProvider::PROVIDER_NAME_FACEBOOK);

        $this->assertNotNull($result);
        $this->assertEquals($createdSocialAccount->getId(), $result->getId());
    }

    public function test_createFromAccount()
    {
        $sampleAccount = new TestAccount();

        $createdSocialAccount = $this->socialAccountService->createFromAccount($sampleAccount, SocialProvider::PROVIDER_NAME_FACEBOOK, '123', new SocialAccessToken('456'), false);

        $this->assertNotNull($createdSocialAccount);
        $this->assertNotNull($createdSocialAccount->getId());
        $this->assertEquals($sampleAccount->getAccountId(), $createdSocialAccount->getAccountId());
        $this->assertEquals($sampleAccount->getEmail(), $createdSocialAccount->getEmail());
        $this->assertEquals(1, $createdSocialAccount->getProviderId());
        $this->assertEquals('123', $createdSocialAccount->getProviderAccountId());
        $this->assertEquals('456', $createdSocialAccount->getProviderAccessToken());
        $this->assertEquals(1, $createdSocialAccount->getStatus());
    }

    public function test_createFromAccount_createEventDispatched()
    {
        $sampleAccount = new TestAccount();
        $testListener = new TestListener();

        $dispatcher = $this->socialAccountService->getEventDispatcher();
        $dispatcher->addListener(SocialAccountCreatedEvent::SOCIAL_ACCOUNT_CREATED, $testListener);

        $this->socialAccountService->createFromAccount($sampleAccount, SocialProvider::PROVIDER_NAME_FACEBOOK, '123', new SocialAccessToken('456'), false);

        $this->assertTrue($testListener->getTriggered());
        $this->assertEquals('Social account created successfully.', $testListener->getReceivedMessage());
    }

    public function test_createFromAccount_linkedEventDispatched()
    {
        $sampleAccount = new TestAccount();
        $testListener = new TestListener();

        $dispatcher = $this->socialAccountService->getEventDispatcher();
        $dispatcher->addListener(SocialAccountLinkedEvent::SOCIAL_ACCOUNT_LINKED, $testListener);

        $this->socialAccountService->createFromAccount($sampleAccount, SocialProvider::PROVIDER_NAME_FACEBOOK, '123', new SocialAccessToken('456'), true);

        $this->assertTrue($testListener->getTriggered());
        $this->assertEquals('Social account was linked successfully.', $testListener->getReceivedMessage());
    }

}