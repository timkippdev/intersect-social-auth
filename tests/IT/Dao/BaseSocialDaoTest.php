<?php

namespace TimKipp\Intersect\Tests\IT\Dao;

use TimKipp\Intersect\Database\Dao\AccountDao;
use TimKipp\Intersect\Social\Dao\SocialAccountDao;
use TimKipp\Intersect\Social\Dao\SocialProviderDao;
use TimKipp\Intersect\Tests\IT\BaseITTest;

abstract class BaseSocialDaoTest extends BaseITTest {

    /** @var AccountDao $accountDao */
    protected $accountDao;

    /** @var SocialAccountDao $socialAccountDao */
    protected $socialAccountDao;

    /** @var SocialProviderDao $socialProviderDao */
    protected $socialProviderDao;

    protected function setUp()
    {
        parent::setUp();

        $this->accountDao = new AccountDao(self::$DB);
        $this->socialAccountDao = new SocialAccountDao(self::$DB);
        $this->socialProviderDao = new SocialProviderDao(self::$DB);
    }

}