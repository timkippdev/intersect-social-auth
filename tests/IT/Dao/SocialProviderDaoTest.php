<?php

namespace TimKipp\Intersect\Tests\IT\Dao;

use TimKipp\Intersect\Tests\Stubs\TestSocialProvider;

class SocialProviderDaoTest extends BaseSocialDaoTest {

    public function test_createRecord()
    {
        $sampleSocialProvider = new TestSocialProvider();

        $createdSocialProvider = $this->socialProviderDao->createRecord($sampleSocialProvider);

        $this->assertNotNull($createdSocialProvider);
        $this->assertNotNull($createdSocialProvider->getProviderId());
        $this->assertEquals($sampleSocialProvider->getProviderName(), $createdSocialProvider->getProviderName());
    }

    public function test_getByName()
    {
        $createdSocialProvider = $this->socialProviderDao->createRecord(new TestSocialProvider());

        $existingSocialProvider = $this->socialProviderDao->getByName($createdSocialProvider->getProviderName());

        $this->assertNotNull($existingSocialProvider);
        $this->assertEquals($createdSocialProvider->getProviderId(), $existingSocialProvider->getProviderId());
        $this->assertEquals($createdSocialProvider->getProviderName(), $existingSocialProvider->getProviderName());
    }

}