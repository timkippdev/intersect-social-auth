<?php

namespace TimKipp\Intersect\Tests\IT;

use PHPUnit\Framework\TestCase;
use TimKipp\Intersect\Helper\SessionManager;
use TimKipp\Intersect\Tests\Stubs\TestAccountService;

abstract class BaseITTest extends TestCase {

    /** @var TestDatabaseAdapter $DB */
    protected static $DB;

    /** @var TestAccountService $accountService */
    protected $accountService;

    /** @var SessionManager $sessionManager */
    protected $sessionManager;

    public static function setUpBeforeClass()
    {
        self::$DB = new TestDatabaseAdapter();
        self::$DB->selectDatabase(DB_NAME);
    }

    protected function setUp()
    {
        parent::setUp();

        unset($_POST);
        unset($_GET);

        $this->accountService = new TestAccountService(self::$DB);
        $this->sessionManager = new SessionManager();

        $this->sessionManager->destroy();
    }

    /**
     * @param null $timestamp
     * @return false|string
     */
    public function getMySQLNow($timestamp = null)
    {
        return date('Y-m-d H:i:s', (is_null($timestamp) ? time() : $timestamp));
    }

}