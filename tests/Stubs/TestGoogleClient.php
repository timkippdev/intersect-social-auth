<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Social\Clients\Google\GoogleClient;

class TestGoogleClient extends GoogleClient {

    public function getOAuthClient()
    {
        return parent::getOAuthClient();
    }

    public function setOAuthClient(\Google_Client $client)
    {
        parent::setOAuthClient($client);
    }

    public function getOAuthService()
    {
        return parent::getOAuthService();
    }

    public function setOAuthService(\Google_Service $service)
    {
        parent::setOAuthService($service);
    }

}