<?php

namespace TimKipp\Intersect\Tests\Stubs;

use Abraham\TwitterOAuth\TwitterOAuth;
use TimKipp\Intersect\Social\Clients\Twitter\TwitterClient;

class TestTwitterClient extends TwitterClient {

    public function getTwitterOAuth()
    {
        return parent::getTwitterOAuth();
    }

    public function setTwitterOAuth(TwitterOAuth $twitterOAuth)
    {
        parent::setTwitterOAuth($twitterOAuth);
    }

}