<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Social\Domain\SocialAccount;

class TestSocialAccount extends SocialAccount {

    public function __construct($accountId)
    {
        $suffix = uniqid();
        
        $this->setId(1);
        $this->setProviderId(1);
        $this->setProviderAccessToken('accessToken_' . $suffix);
        $this->setProviderAccountId('providerAccountId' . $suffix);
        $this->setStatus(1);
        $this->setEmail('email_' . $suffix . '@test.com');
        $this->setAccountId($accountId);
    }

}