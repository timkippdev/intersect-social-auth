<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Social\Domain\SocialProvider;

class TestSocialProvider extends SocialProvider {

    public function __construct()
    {
        $suffix = uniqid();

        $this->setProviderId(1);
        $this->setProviderName('test_' . $suffix);
    }

}