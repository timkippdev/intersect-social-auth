<?php

namespace TimKipp\Intersect\Tests\Unit;

use TimKipp\Intersect\Social\Controllers\SocialLoginController;
use TimKipp\Intersect\Social\Domain\SocialProvider;
use TimKipp\Intersect\Social\Services\SocialAccountService;

class SocialLoginControllerTest extends BaseSocialUnitTest {

    /** @var \PHPUnit_Framework_MockObject_MockObject $socialAccountServiceMock */
    protected $socialAccountServiceMock;

    /** @var SocialLoginController $socialLoginController */
    private $socialLoginController;

    protected function setUp()
    {
        parent::setUp();

        $this->socialAccountServiceMock = $this->getMockBuilder(SocialAccountService::class)->setConstructorArgs(array($this->databaseAdapterMock))->getMock();

        $this->socialLoginController = new SocialLoginController(
            array(
                'databaseAdapter' => $this->databaseAdapterMock,
                'accountService' => $this->accountServiceMock,
                'socialAccountService' => $this->socialAccountServiceMock
            )
        );
    }

    /**
     * @throws \Exception
     */
    public function test_processSocialLoginCallback_facebookDetected()
    {
        $this->socialAccountServiceMock
            ->expects($this->once())
            ->method('isLoginWithFacebookDetected')
            ->willReturn(true);

        $this->socialAccountServiceMock
            ->expects($this->once())
            ->method('loginWithFacebook');

        $this->socialLoginController->processSocialLoginCallback();
    }

    /**
     * @throws \Exception
     */
    public function test_processSocialLoginCallback_twitterDetected()
    {
        $this->socialAccountServiceMock
            ->expects($this->once())
            ->method('isLoginWithTwitterDetected')
            ->willReturn(true);

        $this->socialAccountServiceMock
            ->expects($this->once())
            ->method('loginWithTwitter');

        $this->socialLoginController->processSocialLoginCallback();
    }

    /**
     * @throws \Exception
     */
    public function test_processSocialLoginCallback_googleDetected()
    {
        $this->socialAccountServiceMock
            ->expects($this->once())
            ->method('isLoginWithGoogleDetected')
            ->willReturn(true);

        $this->socialAccountServiceMock
            ->expects($this->once())
            ->method('loginWithGoogle');

        $this->socialLoginController->processSocialLoginCallback();
    }

    /**
     * @throws \Exception
     */
    public function test_processSocialLoginCallback_noProviderDetected()
    {
        $this->socialAccountServiceMock
            ->expects($this->once())
            ->method('isLoginWithFacebookDetected')
            ->willReturn(false);

        $this->socialAccountServiceMock
            ->expects($this->once())
            ->method('isLoginWithTwitterDetected')
            ->willReturn(false);

        $this->socialAccountServiceMock
            ->expects($this->once())
            ->method('isLoginWithGoogleDetected')
            ->willReturn(false);

        $this->socialAccountServiceMock
            ->expects($this->never())
            ->method('loginWithFacebook');

        $this->socialAccountServiceMock
            ->expects($this->never())
            ->method('loginWithTwitter');

        $this->socialAccountServiceMock
            ->expects($this->never())
            ->method('loginWithGoogle');

        $this->socialLoginController->processSocialLoginCallback();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Unsupported social provider
     * @throws \Exception
     */
    public function test_redirectToSocialProvider_unsupportedProvider()
    {
        $this->socialLoginController->redirectToSocialProvider('invalid', true);
    }

    /**
     * @throws \Exception
     */
    public function test_redirectToSocialProvider_facebook()
    {
        $expectedRedirectUrl = 'http://test.com';

        $this->socialAccountServiceMock
            ->expects($this->once())
            ->method('getFacebookLoginUrl')
            ->willReturn($expectedRedirectUrl);

        $rawRedirectUrl = $this->socialLoginController->redirectToSocialProvider(SocialProvider::PROVIDER_NAME_FACEBOOK, true);

        $this->assertEquals($expectedRedirectUrl, $rawRedirectUrl);
    }

    /**
     * @throws \Exception
     */
    public function test_redirectToSocialProvider_twitter()
    {
        $expectedRedirectUrl = 'http://test.com';

        $this->socialAccountServiceMock
            ->expects($this->once())
            ->method('getTwitterLoginUrl')
            ->willReturn($expectedRedirectUrl);

        $rawRedirectUrl = $this->socialLoginController->redirectToSocialProvider(SocialProvider::PROVIDER_NAME_TWITTER, true);

        $this->assertEquals($expectedRedirectUrl, $rawRedirectUrl);
    }

    /**
     * @throws \Exception
     */
    public function test_redirectToSocialProvider_google()
    {
        $expectedRedirectUrl = 'http://test.com';

        $this->socialAccountServiceMock
            ->expects($this->once())
            ->method('getGoogleLoginUrl')
            ->willReturn($expectedRedirectUrl);

        $rawRedirectUrl = $this->socialLoginController->redirectToSocialProvider(SocialProvider::PROVIDER_NAME_GOOGLE, true);

        $this->assertEquals($expectedRedirectUrl, $rawRedirectUrl);
    }

}