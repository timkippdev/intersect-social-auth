<?php

namespace TimKipp\Intersect\Tests\Unit\Services;

use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use TimKipp\Intersect\Event\Event;
use TimKipp\Intersect\Social\Clients\Facebook\FacebookClient;
use TimKipp\Intersect\Social\Clients\Facebook\FacebookConfiguration;
use TimKipp\Intersect\Social\Dao\SocialAccountDao;
use TimKipp\Intersect\Social\Domain\SocialAccessToken;
use TimKipp\Intersect\Social\Domain\SocialAccount;
use TimKipp\Intersect\Social\Domain\SocialProvider;
use TimKipp\Intersect\Social\Domain\SocialUserDetails;
use TimKipp\Intersect\Social\Event\SocialAccountLoginEvent;
use TimKipp\Intersect\Social\Services\SocialAccountService;
use TimKipp\Intersect\Social\Services\SocialProviderService;
use TimKipp\Intersect\Social\SocialClientException;
use TimKipp\Intersect\Tests\Stubs\TestAccount;
use TimKipp\Intersect\Tests\Stubs\TestListener;
use TimKipp\Intersect\Tests\Stubs\TestSocialAccount;
use TimKipp\Intersect\Tests\Unit\BaseSocialUnitTest;

class SocialAccountServiceTest extends BaseSocialUnitTest {

    /** @var SocialAccountService $socialAccountService */
    private $socialAccountService;

    /** @var \PHPUnit_Framework_MockObject_MockObject $socialAccountDaoMock */
    private $socialAccountDaoMock;

    /** @var \PHPUnit_Framework_MockObject_MockObject $socialClientMock */
    private $socialClientMock;

    /** @var \PHPUnit_Framework_MockObject_MockObject $socialProviderServiceMock */
    private $socialProviderServiceMock;
    
    protected function setUp()
    {
        parent::setUp();

        unset($_GET);

        $facebookConfiguration = new FacebookConfiguration('app', 'secret', 'http://localhost');
        $this->socialClientMock = $this->getMockBuilder(FacebookClient::class)->setConstructorArgs(array($facebookConfiguration))->getMock();

        $this->socialAccountDaoMock = $this->getMockBuilder(SocialAccountDao::class)->setConstructorArgs(array($this->databaseAdapterMock))->getMock();
        $this->socialProviderServiceMock = $this->getMockBuilder(SocialProviderService::class)->setConstructorArgs(array($this->databaseAdapterMock))->getMock();

        $this->socialAccountService = new SocialAccountService($this->databaseAdapterMock);
        $this->socialAccountService->setSocialProviderService($this->socialProviderServiceMock);
        $this->socialAccountService->setDao($this->socialAccountDaoMock);
        $this->socialAccountService->setAccountService($this->accountServiceMock);
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage FacebookClient not set properly
     */
    public function test_loginWithFacebook_invalidFacebookClient()
    {
        $this->socialAccountService->loginWithFacebook();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage something went wrong
     */
    public function test_loginWithFacebook_getAccessToken_handleException()
    {
        $facebookClient = $this->getFacebookClientWithMocks();

        $this->socialAccountService->setFacebookClient($facebookClient);

        $facebookClient->getFacebookRedirectLoginHelper()->expects($this->once())
            ->method('getAccessToken')
            ->will($this->throwException(new SocialClientException('something went wrong')));

        $this->socialAccountService->loginWithFacebook();
    }

    public function test_loginWithFacebook_getAccessToken_null()
    {
        $facebookClient = $this->getFacebookClientWithMocks();

        $this->socialAccountService->setFacebookClient($facebookClient);

        $facebookClient->getFacebookRedirectLoginHelper()->expects($this->once())
            ->method('getAccessToken')
            ->will($this->returnValue(null));

        $this->assertFalse($this->socialAccountService->loginWithFacebook());
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Something went wrong retrieving user details from Facebook
     */
    public function test_loginWithFacebook_getResponseFromFacebook_null()
    {
        $facebookClient = $this->getFacebookClientWithMocks();

        $this->socialAccountService->setFacebookClient($facebookClient);

        $facebookClient->getFacebookRedirectLoginHelper()->expects($this->once())
            ->method('getAccessToken')
            ->will($this->returnValue('test'));

        $facebookClient->getFacebook()->expects($this->once())
            ->method('get')
            ->with('/me?fields=id,email,first_name,last_name', 'test')
            ->will($this->returnValue(null));

        $this->socialAccountService->loginWithFacebook();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage A valid Facebook email could not be found
     */
    public function test_loginWithFacebook_getResponseFromFacebook_emptyFacebookUserEmail()
    {
        $facebookClient = $this->getFacebookClientWithMocks();

        $this->socialAccountService->setFacebookClient($facebookClient);

        $facebookClient->getFacebookRedirectLoginHelper()->expects($this->once())
            ->method('getAccessToken')
            ->will($this->returnValue('test'));

        $facebookClient->getFacebook()->expects($this->once())
            ->method('get')
            ->with('/me?fields=id,email,first_name,last_name', 'test')
            ->will($this->returnValue(new FacebookResponse(new FacebookRequest(), '{"email":""}')));

        $this->socialAccountService->loginWithFacebook();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Social account (1) does not have an existing account associated with it. This should not happen ever.
     */
    public function test_loginWithFacebook_accountChecks_existingSocialAccountWithoutNormalAccount()
    {
        $this->socialAccountService->setFacebookClient($this->socialClientMock);

        $accessToken = new SocialAccessToken('test');
        $socialUserDetails = new SocialUserDetails();
        $socialUserDetails->setEmail('test@test.com');

        $this->socialClientMock->expects($this->once())
            ->method('getAccessToken')
            ->will($this->returnValue($accessToken));

        $this->socialClientMock->expects($this->once())
            ->method('getUserDetails')
            ->with($accessToken)
            ->will($this->returnValue($socialUserDetails));

        $this->socialProviderServiceMock->expects($this->once())
            ->method('getByName')
            ->with(SocialProvider::PROVIDER_NAME_FACEBOOK)
            ->will($this->returnValue(new SocialProvider()));

        $this->accountServiceMock->expects($this->once())
            ->method('getByEmail')
            ->with($socialUserDetails->getEmail())
            ->will($this->returnValue(null));

        $this->socialAccountDaoMock->expects($this->once())
            ->method('getByEmailAndProviderId')
            ->will($this->returnValue(new TestSocialAccount(1)));

        $this->socialAccountService->loginWithFacebook();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage There was a problem creating your account.
     */
    public function test_loginWithFacebook_accountChecks_noExistingSocialOrNormalAccount_problemCreatingNewNormalAccount()
    {
        $this->socialAccountService->setFacebookClient($this->socialClientMock);

        $accessToken = new SocialAccessToken('test');
        $socialUserDetails = new SocialUserDetails();
        $socialUserDetails->setEmail('test@test.com');

        $this->socialClientMock->expects($this->once())
            ->method('getAccessToken')
            ->will($this->returnValue($accessToken));

        $this->socialClientMock->expects($this->once())
            ->method('getUserDetails')
            ->with($accessToken)
            ->will($this->returnValue($socialUserDetails));

        $this->socialProviderServiceMock->expects($this->once())
            ->method('getByName')
            ->with(SocialProvider::PROVIDER_NAME_FACEBOOK)
            ->will($this->returnValue(new SocialProvider()));

        $this->accountServiceMock->expects($this->once())
            ->method('getByEmail')
            ->with($socialUserDetails->getEmail())
            ->will($this->returnValue(null));

        $this->socialAccountDaoMock->expects($this->once())
            ->method('getByEmailAndProviderId')
            ->will($this->returnValue(null));

        $this->accountServiceMock->expects($this->once())
            ->method('createWithoutPassword')
            ->will($this->returnValue(null));

        $this->socialAccountService->loginWithFacebook();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Something went wrong creating the new social account
     */
    public function test_loginWithFacebook_accountChecks_noExistingSocialOrNormalAccount_problemCreatingNewSocialAccount()
    {
        $this->socialAccountService->setFacebookClient($this->socialClientMock);

        $createdAccount = new TestAccount();
        $accessToken = new SocialAccessToken('test');
        $socialUserDetails = new SocialUserDetails();
        $socialUserDetails->setEmail('test@test.com');

        $this->socialClientMock->expects($this->once())
            ->method('getAccessToken')
            ->will($this->returnValue($accessToken));

        $this->socialClientMock->expects($this->once())
            ->method('getUserDetails')
            ->with($accessToken)
            ->will($this->returnValue($socialUserDetails));

        $this->socialProviderServiceMock->expects($this->exactly(2))
            ->method('getByName')
            ->with(SocialProvider::PROVIDER_NAME_FACEBOOK)
            ->will($this->returnValue(new SocialProvider()));

        $this->accountServiceMock->expects($this->once())
            ->method('getByEmail')
            ->with($socialUserDetails->getEmail())
            ->will($this->returnValue(null));

        $this->socialAccountDaoMock->expects($this->once())
            ->method('getByEmailAndProviderId')
            ->will($this->returnValue(null));

        $this->accountServiceMock->expects($this->once())
            ->method('createWithoutPassword')
            ->will($this->returnValue($createdAccount));

        $this->socialAccountService->loginWithFacebook();
    }

    public function test_loginWithFacebook_accountChecks_noExistingSocialOrNormalAccount()
    {
        $this->socialAccountService->setFacebookClient($this->socialClientMock);

        $existingAccount = new TestAccount();
        $accessToken = new SocialAccessToken('test');
        $socialUserDetails = new SocialUserDetails();
        $socialUserDetails->setEmail('test@test.com');

        $this->socialClientMock->expects($this->once())
            ->method('getAccessToken')
            ->will($this->returnValue($accessToken));

        $this->socialClientMock->expects($this->once())
            ->method('getUserDetails')
            ->with($accessToken)
            ->will($this->returnValue($socialUserDetails));

        $this->socialProviderServiceMock->expects($this->once())
            ->method('getByName')
            ->with(SocialProvider::PROVIDER_NAME_FACEBOOK)
            ->will($this->returnValue(new SocialProvider()));

        $this->accountServiceMock->expects($this->once())
            ->method('getByEmail')
            ->with($socialUserDetails->getEmail())
            ->will($this->returnValue($existingAccount));

        $this->socialAccountDaoMock->expects($this->once())
            ->method('getByEmailAndProviderId')
            ->will($this->returnValue(new TestSocialAccount(1)));

        $this->accountServiceMock->expects($this->once())
            ->method('login')
            ->with($existingAccount, true)
            ->will($this->returnValue(true));

        $this->assertTrue($this->socialAccountService->loginWithFacebook());
    }

    public function test_loginWithFacebook_accountChecks_noExistingSocialOrNormalAccount_shouldNotCreateNewAccount()
    {
        $this->socialAccountService->setFacebookClient($this->socialClientMock);

        $accessToken = new SocialAccessToken('test');
        $socialUserDetails = new SocialUserDetails();
        $socialUserDetails->setEmail('test@test.com');

        $this->socialClientMock->expects($this->once())
            ->method('getAccessToken')
            ->will($this->returnValue($accessToken));

        $this->socialClientMock->expects($this->once())
            ->method('getUserDetails')
            ->with($accessToken)
            ->will($this->returnValue($socialUserDetails));

        $this->socialProviderServiceMock->expects($this->once())
            ->method('getByName')
            ->with(SocialProvider::PROVIDER_NAME_FACEBOOK)
            ->will($this->returnValue(new SocialProvider()));

        $this->accountServiceMock->expects($this->once())
            ->method('getByEmail')
            ->with($socialUserDetails->getEmail())
            ->will($this->returnValue(null));

        $this->socialAccountDaoMock->expects($this->once())
            ->method('getByEmailAndProviderId')
            ->will($this->returnValue(null));

        $this->assertFalse($this->socialAccountService->loginWithFacebook(true, false));
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Something went wrong creating the new social account
     */
    public function test_loginWithFacebook_accountChecks_noExistingSocialButNormalAccountExists_problemCreatingNewSocialAccount()
    {
        $this->socialAccountService->setFacebookClient($this->socialClientMock);

        $accessToken = new SocialAccessToken('test');
        $socialUserDetails = new SocialUserDetails();
        $socialUserDetails->setEmail('test@test.com');

        $this->socialClientMock->expects($this->once())
            ->method('getAccessToken')
            ->will($this->returnValue($accessToken));

        $this->socialClientMock->expects($this->once())
            ->method('getUserDetails')
            ->with($accessToken)
            ->will($this->returnValue($socialUserDetails));

        $this->socialProviderServiceMock->expects($this->exactly(2))
            ->method('getByName')
            ->with(SocialProvider::PROVIDER_NAME_FACEBOOK)
            ->will($this->returnValue(new SocialProvider()));

        $this->accountServiceMock->expects($this->once())
            ->method('getByEmail')
            ->with($socialUserDetails->getEmail())
            ->will($this->returnValue(null));

        $this->socialAccountDaoMock->expects($this->once())
            ->method('getByEmailAndProviderId')
            ->will($this->returnValue(null));

        $this->accountServiceMock->expects($this->once())
            ->method('createWithoutPassword')
            ->will($this->returnValue(new TestAccount()));

        $this->socialAccountDaoMock
            ->expects($this->once())
            ->method('createRecord')
            ->willReturn(null);

        $this->socialAccountService->loginWithFacebook();
    }

    public function test_loginWithFacebook_accountChecks_noExistingSocialButNormalAccountExists()
    {
//        $socialAccountServiceMock = $this->createMock(SocialAccountService::class);
//
//        $facebookClient = $this->getFacebookClientWithMocks();
//
//        $createdAccount = new TestAccount();
//
//        $this->socialAccountService->setFacebookClient($facebookClient);
//        $this->socialAccountService->setSocialAccountService($socialAccountServiceMock);
//
//        $facebookClient->getFacebookRedirectLoginHelper()->expects($this->once())
//            ->method('getAccessToken')
//            ->will($this->returnValue('test'));
//
//        $facebookClient->getFacebook()->expects($this->once())
//            ->method('get')
//            ->with('/me?fields=id,email,first_name,last_name', 'test')
//            ->will($this->returnValue(new FacebookResponse(new FacebookRequest(), '{"id":"123","email":"test@test.com"}')));
//
//        $this->accountDaoMock
//            ->expects($this->once())
//            ->method('getByEmail')
//            ->willReturn($createdAccount);
//
//        $socialAccountServiceMock->expects($this->once())
//            ->method('getByEmailAndProviderName')
//            ->with('test@test.com', SocialProvider::PROVIDER_NAME_FACEBOOK)
//            ->will($this->returnValue(null));
//
//        $socialAccountServiceMock->expects($this->once())
//            ->method('createFromAccount')
//            ->with($createdAccount, SocialProvider::PROVIDER_NAME_FACEBOOK, '123', new SocialAccessToken('test'))
//            ->willReturn(new SocialAccount());
//
//        $this->addValidLoginExpects();
//
//        $this->assertTrue($this->socialAccountService->loginWithFacebook());


        $this->socialAccountService->setFacebookClient($this->socialClientMock);

        $existingAccount = new TestAccount();
        $accessToken = new SocialAccessToken('test');
        $socialUserDetails = new SocialUserDetails();
        $socialUserDetails->setEmail('test@test.com');

        $this->socialClientMock->expects($this->once())
            ->method('getAccessToken')
            ->will($this->returnValue($accessToken));

        $this->socialClientMock->expects($this->once())
            ->method('getUserDetails')
            ->with($accessToken)
            ->will($this->returnValue($socialUserDetails));

        $this->socialProviderServiceMock->expects($this->exactly(2))
            ->method('getByName')
            ->with(SocialProvider::PROVIDER_NAME_FACEBOOK)
            ->will($this->returnValue(new SocialProvider()));

        $this->accountServiceMock->expects($this->once())
            ->method('getByEmail')
            ->with($socialUserDetails->getEmail())
            ->will($this->returnValue($existingAccount));

        $this->socialAccountDaoMock->expects($this->once())
            ->method('getByEmailAndProviderId')
            ->will($this->returnValue(null));

        $this->socialAccountDaoMock
            ->expects($this->once())
            ->method('createRecord')
            ->willReturn(new SocialAccount());

        $this->accountServiceMock->expects($this->once())
            ->method('login')
            ->with($existingAccount, true)
            ->will($this->returnValue(true));

        $this->assertTrue($this->socialAccountService->loginWithFacebook());
    }

    public function test_loginWithFacebook_eventDispatched()
    {
        $this->socialAccountService->setFacebookClient($this->socialClientMock);

        $existingAccount = new TestAccount();
        $testListener = new TestListener();
        $accessToken = new SocialAccessToken('test');
        $socialUserDetails = new SocialUserDetails();
        $socialUserDetails->setEmail('test@test.com');

        $this->socialAccountService->getEventDispatcher()->addListener(SocialAccountLoginEvent::SOCIAL_ACCOUNT_LOGIN, $testListener);

        $this->socialClientMock->expects($this->once())
            ->method('getAccessToken')
            ->will($this->returnValue($accessToken));

        $this->socialClientMock->expects($this->once())
            ->method('getUserDetails')
            ->with($accessToken)
            ->will($this->returnValue($socialUserDetails));

        $this->socialProviderServiceMock->expects($this->once())
            ->method('getByName')
            ->with(SocialProvider::PROVIDER_NAME_FACEBOOK)
            ->will($this->returnValue(new SocialProvider()));

        $this->accountServiceMock->expects($this->once())
            ->method('getByEmail')
            ->with($socialUserDetails->getEmail())
            ->will($this->returnValue($existingAccount));

        $this->socialAccountDaoMock->expects($this->once())
            ->method('getByEmailAndProviderId')
            ->will($this->returnValue(new TestSocialAccount(1)));

        $this->accountServiceMock->expects($this->once())
            ->method('login')
            ->with($existingAccount, true)
            ->will($this->returnValue(true));

        $this->assertTrue($this->socialAccountService->loginWithFacebook());
        $this->assertTrue($testListener->getTriggered());
        $this->assertEquals('Social account logged in successfully.', $testListener->getReceivedMessage());
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage TwitterClient not set properly
     */
    public function test_loginWithTwitter_invalidTwitterClient()
    {
        $this->socialAccountService->loginWithTwitter();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage something went wrong
     */
    public function test_loginWithTwitter_getAccessToken_handleException()
    {
        $twitterClient = $this->getTwitterClientWithMocks();

        $this->socialAccountService->setTwitterClient($twitterClient);

        $_GET['oauth_token'] = 'oauth_token';
        $_GET['oauth_verifier'] = 'oauth_verifier';

        $this->sessionManagerMock
            ->expects($this->exactly(2))
            ->method('readAndClear')
            ->willReturn('oauth_token');

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('oauth')
            ->with('oauth/access_token', array(
                'oauth_verifier' => 'oauth_verifier'
            ))
            ->will($this->throwException(new SocialClientException('something went wrong')));

        $this->socialAccountService->loginWithTwitter();
    }

    public function test_loginWithTwitter_getAccessToken_null()
    {
        $twitterClient = $this->getTwitterClientWithMocks();

        $this->socialAccountService->setTwitterClient($twitterClient);

        $_GET['oauth_token'] = 'oauth_token';
        $_GET['oauth_verifier'] = 'oauth_verifier';

        $this->sessionManagerMock
            ->expects($this->exactly(2))
            ->method('readAndClear')
            ->willReturn('oauth_token');

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('oauth')
            ->with('oauth/access_token', array(
                'oauth_verifier' => 'oauth_verifier'
            ))
            ->willReturn(null);

        $this->assertFalse($this->socialAccountService->loginWithTwitter());
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Something went wrong retrieving user details from Twitter
     */
    public function test_loginWithTwitter_getResponseFromTwitter_null()
    {
        $twitterClient = $this->getTwitterClientWithMocks();

        $this->socialAccountService->setTwitterClient($twitterClient);

        $_GET['oauth_token'] = 'oauth_token';
        $_GET['oauth_verifier'] = 'oauth_verifier';

        $this->sessionManagerMock
            ->expects($this->exactly(2))
            ->method('readAndClear')
            ->willReturn('oauth_token');

        $twitterClient->getTwitterOAuth()
            ->expects($this->exactly(2))
            ->method('setOauthToken');

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('oauth')
            ->with('oauth/access_token', array(
                'oauth_verifier' => 'oauth_verifier'
            ))
            ->willReturn(array(
                'oauth_token' => 'token',
                'oauth_token_secret' => 'secret'
            ));

        $twitterClient->getTwitterOAuth()
            ->expects($this->once())
            ->method('get')
            ->with('account/verify_credentials', array(
                'include_email' => 'true',
                'include_entities' => 'false',
                'skip_status' => 'true'
            ))
            ->willReturn(null);

        $this->socialAccountService->loginWithTwitter();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage GoogleClient not set properly
     */
    public function test_loginWithGoogle_invalidGoogleClient()
    {
        $this->socialAccountService->loginWithGoogle();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage something went wrong
     */
    public function test_loginWithGoogle_getAccessToken_handleException()
    {
        $googleClient = $this->getGoogleClientWithMocks();

        $this->socialAccountService->setGoogleClient($googleClient);

        $_GET['code'] = 'code';

        $googleClient->getOAuthClient()
            ->expects($this->once())
            ->method('fetchAccessTokenWithAuthCode')
            ->with('code')
            ->will($this->throwException(new SocialClientException('something went wrong')));

        $this->socialAccountService->loginWithGoogle();
    }

    public function test_loginWithGoogle_getAccessToken_null()
    {
        $googleClient = $this->getGoogleClientWithMocks();

        $this->socialAccountService->setGoogleClient($googleClient);

        $_GET['code'] = 'code';

        $googleClient->getOAuthClient()
            ->expects($this->once())
            ->method('fetchAccessTokenWithAuthCode')
            ->with('code')
            ->willReturn(null);

        $this->assertFalse($this->socialAccountService->loginWithGoogle());
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Something went wrong retrieving user details from Google
     */
    public function test_loginWithGoogle_getResponseFromGoogle_null()
    {
        $googleClient = $this->getGoogleClientWithMocks(true);

        $this->socialAccountService->setGoogleClient($googleClient);

        $_GET['code'] = 'code';

        $googleClient->getOAuthClient()
            ->expects($this->once())
            ->method('fetchAccessTokenWithAuthCode')
            ->with('code')
            ->willReturn(array(
                'access_token' => 'token'
            ));

        $this->socialAccountService->loginWithGoogle();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage A valid Google email could not be found
     */
    public function test_loginWithGoogle_getResponseFromGoogle_emptyGoogleEmail()
    {
        $googleClient = $this->getGoogleClientWithMocks(false, true);

        $this->socialAccountService->setGoogleClient($googleClient);

        $_GET['code'] = 'code';

        $googleClient->getOAuthClient()
            ->expects($this->once())
            ->method('fetchAccessTokenWithAuthCode')
            ->with('code')
            ->willReturn(array(
                'access_token' => 'token'
            ));

        $this->socialAccountService->loginWithGoogle();
    }

    public function test_isLoginWithFacebookDetected()
    {
        $this->assertFalse($this->socialAccountService->isLoginWithFacebookDetected());

        $_GET['code'] = 'code';
        $_GET['state'] = 'state';

        $this->assertTrue($this->socialAccountService->isLoginWithFacebookDetected());
    }

    public function test_isLoginWithTwitterDetected()
    {
        $this->assertFalse($this->socialAccountService->isLoginWithTwitterDetected());

        $_GET['oauth_token'] = 'oauth_token';
        $_GET['oauth_verifier'] = 'oauth_verifier';

        $this->assertTrue($this->socialAccountService->isLoginWithTwitterDetected());
    }

    public function test_isLoginWithGoogleDetected()
    {
        $this->assertFalse($this->socialAccountService->isLoginWithGoogleDetected());

        $_GET['code'] = 'code';

        $this->assertTrue($this->socialAccountService->isLoginWithGoogleDetected());
    }

    private function addValidLoginExpects()
    {
        $this->accountDaoMock
            ->expects($this->once())
            ->method('checkIfEmailOrUsernameValidAndActive')
            ->willReturn(true);

        $this->accountDaoMock
            ->expects($this->any())
            ->method('getAllBy')
            ->willReturn(new Account());

        $this->encryptorServiceMock
            ->expects($this->any())
            ->method('isValid')
            ->willReturn(true);

        $this->accountDaoMock
            ->expects($this->once())
            ->method('updateRecord')
            ->willReturn(new Account());

        $this->sessionManagerMock
            ->expects($this->once())
            ->method('write');
    }

}