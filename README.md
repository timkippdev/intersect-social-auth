# Intersect Social Auth
**Intersect Social Auth** provides

## Changelog
See `CHANGELOG.md` for all current and released features/changes

## Source
https://bitbucket.org/timkippdev/intersect-social-auth

## Installation via Composer
Include the following code snippet inside your project `composer.json` file (update if necessary)
```
"repositories": [
  {
    "type": "vcs",
    "url": "https://bitbucket.org/timkippdev/intersect-social-auth"
  }
],
"require" : {
  "timkipp/intersect-social-auth" : "^1.0.0"
}
```

## Usage
